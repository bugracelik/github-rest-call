package com.bugra.githubrestcall;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GithubRestCallApplication {

	public static void main(String[] args) {
		SpringApplication.run(GithubRestCallApplication.class, args); // test
	}

}
