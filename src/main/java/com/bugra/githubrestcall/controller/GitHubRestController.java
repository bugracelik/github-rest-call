package com.bugra.githubrestcall.controller;


import com.bugra.githubrestcall.domain.GithubDomainPerson;
import com.bugra.githubrestcall.model.GithubPersonDto;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class GitHubRestController {

    @GetMapping("github-rest-call/{name}")
    public ResponseEntity<String> foo(@PathVariable String name) throws JsonProcessingException {

        //HTTP call yapabilmek için RestTemplete alınır
        RestTemplate restTemplate = new RestTemplate();
        //istek atılır
        String url = String.format("https://api.github.com/users/%s", name);
        ResponseEntity<String> response = restTemplate.getForEntity(url, String.class);
        //responsun bodysi alınır(Responsun body si ve headeri string dir"
        String responseBodyJsonFormat = response.getBody();

        //deseri edicem,string to model
        ObjectMapper objectMapper = new ObjectMapper();
        GithubDomainPerson githubPerson = objectMapper.readValue(responseBodyJsonFormat, GithubDomainPerson.class);

        //kullanıcıya dönmek için GithubPersonDto hazırlanır
        GithubPersonDto githubPersonDto = new GithubPersonDto();
        githubPersonDto.setId(githubPerson.getId());
        githubPersonDto.setName(githubPerson.getName());
        githubPersonDto.setUrl(githubPerson.getUrl());


        //seri edicem json formatına, model to string(json)
        String githubPersonDtoSerialize = objectMapper.writeValueAsString(githubPersonDto);

        //response entity oluşturulur


        return new ResponseEntity<>(githubPersonDtoSerialize, HttpStatus.OK);

    }

    @GetMapping("github-rest-call/spring-helping/{userName}")
    public GithubPersonDto getUserInfoFromGithubPublicApi(@PathVariable("userName") String userName) {

        // ayrıntılardan kurtaralım refactor ede ede itere ede, kod konuşuyor, fikir
        GithubApiCallServiceClient githubApiCallServiceClient = new GithubApiCallServiceClient(); // 1 tane client alınır github client

        return githubApiCallServiceClient.getUserInfoWithUserName(userName);
    }


}
