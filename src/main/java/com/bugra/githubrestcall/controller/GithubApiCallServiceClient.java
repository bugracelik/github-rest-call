package com.bugra.githubrestcall.controller;

import com.bugra.githubrestcall.model.GithubPersonDto;
import org.springframework.web.client.RestTemplate;

public class GithubApiCallServiceClient {

    public GithubPersonDto getUserInfoWithUserName(String userName)
    {
        GithubPersonDto githubPersonDto = new GithubPersonDto();

        // bilgisayarın anlauacağo kod
        // programcı döküman okur ( 1 saat ) öyle anlar.
        RestTemplate restTemplate = new RestTemplate();
        String formattedUrlForGithubApiCall = String.format("https://api.github.com/users/%s", userName);
        githubPersonDto = restTemplate.getForObject(formattedUrlForGithubApiCall, GithubPersonDto.class); // java 13 ile gelen yeni bir özellik, jcp, jep ile dile yeni kazanımlar.

        return githubPersonDto;
    }
}
